library(Amelia)
library(VIM)
library(methods)
library(imputeR)
library(rpart)
library(mice)
library(caret)
library(parallel)
library(Metrics)

args <- commandArgs(trailingOnly = TRUE)  # accept arguments from command line
original_dataset <- read.csv(as.character(args[1]),header = FALSE)  # Load the original dataset

##Global Variables
method_list <- list()
method_list[[1]] <- "EM"
method_list[[2]] <- "Mean"
method_list[[3]] <- "Regression"
method_list[[4]] <- "KNN"
method_list[[5]] <- "CART"
method_list[[6]] <- "NoImputation"
metrics <- vector(mode="list") #Contains metrics measurement, retrieve values by name e.x. metrics[["mean"]]


# Preprocessing for CART analysis, convert ring number in age label
# Especially designed for the abalone dataset
ClusterRings <- function(dataset){
  dataset[[9]] <- mcmapply(dataset[[9]], FUN = round)
  dataset[[9]] <- mcmapply(dataset[[9]],
                           FUN=function(x) ifelse(is.na(x), x, ifelse (x<=8, "Young", ifelse (x >= 11, "Old", "Adult"))))
  dataset[[9]] <- as.factor(dataset[[9]])
  return (dataset)
}


#Convert the age label to 1,2,3 integers (for Spearman Rank Correlation)
AgeLabelToInteger <- function(value_list){
  return(ifelse(value_list=="Young",1,ifelse(value_list=="Adult",2,3)))
}


#RESULTS.csv file parameters, header is specific to the abalone dataset
filename <- "results.csv"
header <- "ID,GROUP,IMP_METHOD,COUNT_I$V1,COUNT_M$V1,COUNT_F$V1,RMSE$V2,RMSE$V3,RMSE$V4,RMSE$V5,RMSE$V6,RMSE$V7,RMSE$V8,RMSE$V9,MEAN$V2,MEAN$V3,MEAN$V4,MEAN$V5,MEAN$V6,MEAN$V7,MEAN$V8,MEAN$V9,VAR$V2,VAR$V3,VAR$V4,VAR$V5,VAR$V6,VAR$V7,VAR$V8,VAR$V9,CART_SC"

##Imputation Functions
MeanImputation <- function(dataset){
  for (i in 1:ncol(dataset)){
    if (is.factor(dataset[[i]])){  # Categorical attributes are imputed with the most frequent value
      
      most_frequent <- which.max(table(dataset[[i]]))
      dataset[[i]] <- as.integer(dataset[[i]])
      
      dataset[[i]] <- ifelse(is.na(dataset[[i]]), most_frequent, dataset[[i]])
      
      # Convert numbers back to factors (ifelse is automatically converting to numbers)
      
      dataset[[i]] <- mcmapply(dataset[[i]],
          FUN = function(x) if(is.numeric(x)) levels(original_dataset[[i]])[x])
      dataset[[i]] <- as.factor(dataset[[i]])
    }
    else{
      mean_val <- mean(dataset[[i]],na.rm = TRUE)  # use the arithmetic mean
      dataset[[i]] <- ifelse(is.na(dataset[[i]]), mean_val,dataset[[i]])
    }
  }
  
  return(dataset)
}
#Linear Regression: every non missing variable is used as parameter
RegressionImputation <- function(dataset){
  columns_with_missing<-c()
  columns_sum_of_missing <- apply(dataset, 2, function(x) sum(is.na(x)))
  for (c in 1 : length(columns_sum_of_missing)){
    if (columns_sum_of_missing[c] > 0){  # if this column has missing values
      columns_with_missing <- append(columns_with_missing, c)  #save the index
    } 
  }

  # build the string for the regression formula using the name of the never missing attributes
  non_missing_columns <- setdiff(c(1 : ncol(dataset)), columns_with_missing)
  non_missing_columns <- sapply(non_missing_columns, function(x) paste("V",x,sep=""))
  for (col_mis in columns_with_missing){
    formula <- as.formula(paste("V", col_mis, "~",paste(non_missing_columns, collapse = "+", sep=""), sep=""))
    dataset <- regressionImp(formula,dataset)
  }
  return (dataset)
}
CARTBasedImputation <- function(dataset){
  # returns a list of the imputed values only!! (not columns)
  result <- mice(data = dataset, method = "cart", m = 1, printFlag=FALSE)
  
  #substitute the imputed value in the respective column, in the exact position
  for (col in names(dataset)){
    if (is.null(result$imp[[col]]) == FALSE){
      iterator <- 1
      for (index in 1:nrow(dataset)){
        if (is.na(dataset[[col]][[index]])){
          dataset[[col]][[index]] <- result$imp[[col]][[1]][iterator]
          iterator <- iterator + 1
         }
      }
    }
  }
  return(dataset)
}

# CART analysis
# Classifying the abalone by age
BuildCART <- function(trainSet, response_index, testSet){
  
  # Divide predictors from response for training
  train_data_predictors <- trainSet[setdiff(1:ncol(trainSet),response_index)]
  train_data_response <- trainSet[[response_index]]
  # train the model
  ctrl <- trainControl(method = "repeatedcv", repeats=3) # 10-fold cross validation for 3 times
  tree <- train(train_data_predictors, train_data_response,
                method="rpart", tuneLength = 7 , metric ="Accuracy",
                maximize=TRUE, trControl = ctrl)
  
  #Predictions on the test set and compute Spearmann (SC)
  predictions <- predict.train(type = "raw",object = tree,newdata = testSet)
  metrics[["CART_SC"]] <- cor(x = AgeLabelToInteger(predictions), y = AgeLabelToInteger(testSet[[9]]), method = "spearman")
  return(metrics)
}


##Aux Functions
# Retrieve categorical column index
ComputeCategoricalIndexes <- function(dataset){
  index_list <- list()
  cat_attributes_index <- sapply(dataset,is.factor)
  for ( index in 1:length(cat_attributes_index)){
    if(cat_attributes_index[index]){
      index_list<-append(index_list,index)
    }
  }
  
  return(index_list)
}
ConvertIntegerToNumeric <- function(dataset){
  values <- sapply(dataset,is.integer)
  for (i in 1:length(values)){
    if (values[i]){
      dataset[[i]] <- as.numeric(dataset[[i]])
    }
  }
  return (dataset)
}
# Write to file aux functions. Offload every calculated metrics
ReportSample <- function(imputation_list, id, group, accuracy_list, distortion_list, frequency_list, tree_list){
  for (sample_index in 1:length(imputation_list)){
    rmse_line <- character(0)
    mean_line <- character(0)
    var_line  <- character(0)
    cart_line <- character(0)
    catvar_line <- character(0)
    imputation_line <- paste(id, group, method_list[[sample_index]], sep=",")  # META INFO

    #Write the frequency of categorical columns
    sample_list <- frequency_list[[sample_index]]
    catvar_line <- paste(catvar_line, sample_list[["CAT_FREQ"]][["I"]], 
      sample_list[["CAT_FREQ"]][["M"]], sample_list[["CAT_FREQ"]][["F"]], sep=",")
    
    #Write the RMSE of each column 
    sample_list <- accuracy_list[[sample_index]]
    for (col_index in 1:length(sample_list[["RMSE"]])){
      rmse_line <- paste(rmse_line, sample_list[["RMSE"]][[col_index]], sep=",")
    }
    
    #Write the Mean & Variance of each column
    sample_list <- distortion_list[[sample_index]]
    for (col_index in 1:length(sample_list[["Mean"]])){
      mean_line <- paste(mean_line, sample_list[["Mean"]][[col_index]], sep=",")
      var_line <- paste(var_line, sample_list[["Var"]][[col_index]], sep=",")
    }

    #Write the CART metrics line 
    sample_list <- tree_list[[sample_index]]
    cart_line <- paste(cart_line, sample_list[["CART_SC"]], sep=",")
    final_line <- paste(imputation_line, catvar_line, rmse_line, mean_line, var_line, cart_line, sep="")
    
    writeLines(final_line, fileConn) # Write to file
  }
}

#Metrics Functions
#compute mean and variance of numerical only attributes
ComputeDistortionMetrics <- function (dataset){
  continuos_att_index <- setdiff(c(1:ncol(dataset)),unlist(ComputeCategoricalIndexes(dataset)))
  dataset <- dataset[c(continuos_att_index)]

  metrics[["Mean"]] <- mcmapply(FUN = mean, dataset, na.rm=TRUE)
  metrics[["Var"]]  <- mcmapply(FUN = function(x) 
    var(x, na.rm = TRUE),dataset)
  return(metrics)
}
#Compute RMSE of numerical attributes, against the original values of the complete dataset
ComputeAccuracyMetrics <- function (dataset,true_dataset){
  continuos_att_index <- setdiff(c(1:ncol(dataset)),unlist(ComputeCategoricalIndexes(dataset)))
  dataset <- dataset[c(continuos_att_index)]
  metrics[["RMSE"]] <<- mcmapply(FUN =  function(x)
    ifelse(sum(is.na(dataset[[x]])>0),NA,rmse(predicted = dataset[[x]], actual =  true_dataset[[x]])), names(dataset))
  return(metrics)
}

# returns a list of lists: categorical variables and count of their levels
ComputeCatFrequency <- function (dataset){

  metrics[["CAT_FREQ"]][["I"]] <- table(dataset[[1]])["I"]
  metrics[["CAT_FREQ"]][["M"]] <- table(dataset[[1]])["M"]
  metrics[["CAT_FREQ"]][["F"]] <- table(dataset[[1]])["F"]
  return(metrics)
}

sample_list <- list.files("Datasets")  # lists the datasets in the folder
cat(paste("FOUND ", length(sample_list), "samples\n"))

  
##Initialize result csv file
fileConn<-file(filename,open = "a")
writeLines(header, fileConn)

##Main Flow
for (s in sample_list){
  ## Extract Metainformation
  miss_sample <- read.csv(paste("Datasets/",s, sep=""),header=FALSE)
  miss_sample <- ConvertIntegerToNumeric(miss_sample) # The sample to be imputed and analyzed
  meta_info <- sub(".csv", "", s)  # remove file extension
  meta_info <- strsplit(meta_info, "-")  # extract single metadata
  id <- as.numeric(meta_info[[1]][1])  #sample id
  group <- paste(meta_info[[1]][2],meta_info[[1]][3],sep="-")  # sample group
  cat(paste("processing",id,group, "\n"))
  num_col <- ncol(miss_sample)
  
  ##define test/Training
  inTrain <- createDataPartition(miss_sample[[9]], p = 0.8, list = FALSE) #Select random 80% to get TRAINING set
  trainingSet <- miss_sample[inTrain,]
  testSet <- miss_sample[-inTrain,]
  miss_sample <- trainingSet  # Imputation and measurement is now performed only for the TRAINING set!
  
  ## 5 imputations in parallel
  s1 <- mcparallel(amelia(miss_sample, m=1, noms=1)$imputation$imp1) # EM imputation (requires index of cat.variables)
  s2 <- mcparallel(MeanImputation(miss_sample))
  s3 <- mcparallel(RegressionImputation(miss_sample)[c(1:num_col)])
  s4 <- mcparallel(kNN(miss_sample)[c(1:num_col)]) # KNN imputation
  s5 <- mcparallel(CARTBasedImputation(miss_sample))
  imputed_sample <- mccollect(list(s1, s2, s3, s4,s5))
  imputed_sample[[6]] <- miss_sample # the non imputed dataset
  
  print("IMPUTATION COMPLETE....proceeding to compute distortion metrics")
  
  ##Parallel Compute distortion metrics (distribution and indexes distortion)
  distortion <- list()
  for (i in 1:length(method_list)){
    distortion[[i]] <- mcparallel(ComputeDistortionMetrics(imputed_sample[[i]]))
  }
  distortion_list <- mccollect(distortion)

  ##Parallel compute frequency of categorical attribute
  frequency <- list()
  for (i in 1:length(method_list)){
    frequency[[i]] <- mcparallel(ComputeCatFrequency(imputed_sample[[i]]))
  }
  frequency_list <- mccollect(frequency)
  

  ##Parallel Compute imputation accuracy (RMSE), requires the same rows of the original dataset to compure RMSE!
  accuracy <- list()
  for (i in 1:length(method_list)){
    accuracy[[i]] <- mcparallel(ComputeAccuracyMetrics(imputed_sample[[i]], original_dataset[inTrain,]))
  }
  accuracy_list <- mccollect(accuracy)

  #Preprocess of the datasets, both training and test
  for (i in 1:length(imputed_sample)){
    imputed_sample[[i]] <- ClusterRings(imputed_sample[[i]])
  }
  testSet <- ClusterRings(testSet)
  print("MEASUREMENT complete....building CART")
  
  # CART analsysis
  tree <- list()
  for (i in 1:length(method_list)){
    tree[[i]] <- mcparallel(BuildCART(imputed_sample[[i]],9,testSet))
  }
  tree_list <- mccollect(tree)

  print("CART complete....writing to file")

  # For each sample write 1 entry per imputed dataset
  ReportSample(imputed_sample, id, group, accuracy_list, distortion_list,frequency_list, tree_list)
}
close(fileConn)

  