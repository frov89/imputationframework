# TO DO 

## General ##
* Automatize the whole framework: create a scheduler that runs the operations in sequence and document the steps

## Flow ##

* Generalize: build header automatically from the metadata
* Generalize: compute frequency is now (temporary) specific only for the abalone dataset
* Add imputation method: function needed to add additional imputation methods
* Add metrics: function needed to add additional metrics

## Missing Generator ##
* Generalize: automatically calculate the ideal number of columns in which create missing values 

* Generalize: last minute fix -> right now missing values will never be created in column 9. Fix this and include it as a parameter (ex. index of never-missing-variables)


## Manipulation Check ##

* Add written output: csv or file with the results and ids of the files
* Improvement: reengeneering for parallel computation of the proportional odds model
* Improvement: integrate the mechanism and the pattern test in a unique routine
* Generalize: extend the pattern test for dataset with more than 2 coupled columns for the disjoint pattern